import { Component, OnInit } from '@angular/core';
import { Scrumuser } from '../scrumuser';
import { ScrumdataService } from '../scrumdata.service';
import { Router } from '@angular/router';
// import { Scrumuser } from '../scrumuser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  public scrumUserLoginData = new Scrumuser('','','','','');
  constructor(private _scrumdataservice: ScrumdataService, private _router: Router) {}
  feedbk='';

  ngOnInit(): void {
  }

  onLoginSubmit() {
    this._scrumdataservice.login(this.scrumUserLoginData).subscribe(
      data => {
        console.log('Success!', data),
        localStorage.setItem('token', data.token),
        this._router.navigate(['/scrumboard', data['project_id']])
      },
      error => {
        console.log('Error!', error)
        this.feedbk = 'Login failed'
      }
    )
  }

}
