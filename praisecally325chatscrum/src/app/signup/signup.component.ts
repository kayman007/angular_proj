import { Component, OnInit } from '@angular/core';
import { Scrumuser } from '../scrumuser';
import { ScrumdataService } from '../scrumdata.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  public response:any;
  constructor(private _scrumdataservice: ScrumdataService) {}

  ngOnInit(): void {
  }
  usertype = ['developer', 'owner'];
  scrumUserModel = new Scrumuser('', '', '','','');

    onSubmit() {
      this._scrumdataservice.signup(this.scrumUserModel).subscribe(
        data => {
          this.response = 'Your account was created successfully'
        },
        error => {

        }, 
          
      )
    }

}