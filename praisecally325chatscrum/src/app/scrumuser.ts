export class Scrumuser {
    constructor(
        public email: string,
        public fullname: string,
        public password: string,
        public projectname: string,
        public projname: string
    ){}
}
